<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectPath = '/products';

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $socialUser = Socialite::driver('github')->user();

        $email = $socialUser->email;

        $user = User::where(['email' => $email])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->user['login'],
                'email' => $socialUser->email,
                'password' => str_random(8),
            ]);
        }

        Auth::login($user);

        return redirect($this->redirectPath);
    }
}
