@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Products</div>

                <div class="card-body">
                    <h2>{{ $product->name }}</h2>
                    <span>Price: {{ $product->price }}</span>
                </div>
                @canany(['update', 'delete'], $product)
                    <div>
                        <a href="/products/{{ $product->id }}/edit">Edit</a>
                        <form method="POST" action="{{ url('/products/' . $product->id . '/delete') }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-primary">
                                Delete
                            </button>
                        </form>
                    </div>
                @endcanany
            </div>
        </div>
    </div>
</div>
@endsection
