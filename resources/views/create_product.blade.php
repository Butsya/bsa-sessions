@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Products</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('store-product') }}">
                        <h2>Product Form</h2>
                        <span>
                            Product name: <input name="name" type="text" placeholder="Product name" value="">
                        </span>
                        <br>
                        <span>
                            Price: <input name="price" type="text" placeholder="Product price" value="">
                        </span>
                        <br>
                        <br>
                        <br>
                        @csrf
                        <div>
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
