<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group([
    'prefix' => '/products',
], function () {
    Route::get('/', 'ProductController@index');
    Route::get('/add', 'ProductController@create')->name('add-product');
    Route::post('/store', 'ProductController@store')->name('store-product');
    Route::get('/{id}/edit', 'ProductController@edit');
    Route::put('/{id}/edit', 'ProductController@update');
    Route::delete('/{id}/delete', 'ProductController@destroy');
    Route::get('/{id}', 'ProductController@show');
});

Route::get('/login/github', 'SocialAuthController@redirectToProvider')->name('github-login');
Route::get('/auth/github/callback', 'SocialAuthController@handleProviderCallback');

